import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-management',
  templateUrl: './post-management.component.html',
  styleUrls: ['./post-management.component.css']
})
export class PostManagementComponent implements OnInit {

  isShow = true;
  isShow2 = true;
  isShow3 = true;
  isShow4 = true;
  isShow5 = true;
  isShow6 = true;
  isShow7 = true;
  toggleDisplay() {
    this.isShow4 = true;
    this.isShow2 = true;
    this.isShow3 = true;
    this.isShow5 = true;
    this.isShow6 = true;
    this.isShow7 = true;
    this.isShow = !this.isShow;
  }
  toggleDisplay2() {
    this.isShow3 = true;
    this.isShow = true;
    this.isShow4 = true;
    this.isShow5 = true;
    this.isShow6 = true;
    this.isShow7 = true;
    this.isShow2 = !this.isShow2;
  }
  toggleDisplay3() {
    this.isShow = true;
    this.isShow2 = true;
    this.isShow4 = true;
    this.isShow5 = true;
    this.isShow6 = true;
    this.isShow7 = true;
    this.isShow3 = !this.isShow3;
  }
  toggleDisplay4() {
    this.isShow = true;
    this.isShow2 = true;
    this.isShow3 = true;
    this.isShow5 = true;
    this.isShow6 = true;
    this.isShow7 = true;
    this.isShow4 = !this.isShow4;
  }
  toggleDisplay5() {
    this.isShow = true;
    this.isShow2 = true;
    this.isShow3 = true;
    this.isShow6 = true;
    this.isShow4 = true;
    this.isShow7 = true;
    this.isShow5 = !this.isShow5;
  }
  toggleDisplay6() {
    this.isShow = true;
    this.isShow2 = true;
    this.isShow3 = true;
    this.isShow5 = true;
    this.isShow4 = true;
    this.isShow7 = true;
    this.isShow6 = !this.isShow6;
  }
  toggleDisplay7() {
    this.isShow = true;
    this.isShow2 = true;
    this.isShow3 = true;
    this.isShow5 = true;
    this.isShow4 = true;
    this.isShow6 = true;
    this.isShow7 = !this.isShow7;
  }
  ngOnInit(): void {
  }

}
