import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  isShow = false;
  isShow2 = true;
  isShow3 = true;
  isShow4 = true;
  toggleDisplay() {
    this.isShow2 = true;
    this.isShow3 = true;
    this.isShow4 = true;
    this.isShow = !this.isShow;
  }
  toggleDisplay2() {
    this.isShow3 = true;
    this.isShow = true;
    this.isShow4 = true;
    this.isShow2 = !this.isShow2;
  }
  toggleDisplay3() {
    this.isShow = true;
    this.isShow4 = true;
    this.isShow2 = true;
    this.isShow3 = !this.isShow3;
  }
  toggleDisplay4() {
    this.isShow = true;
    this.isShow3 = true;
    this.isShow2 = true;
    this.isShow4 = !this.isShow4;
  }

  ngOnInit(): void {
  }

}
