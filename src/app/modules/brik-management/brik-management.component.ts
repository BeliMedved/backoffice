import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-brik-management',
  templateUrl: './brik-management.component.html',
  styleUrls: ['./brik-management.component.css']
})
export class BrikManagementComponent implements OnInit {

  isShow = true;
  isShow2 = true;
  isShow3 = true;
  toggleDisplay() {
    this.isShow2 = true;
    this.isShow3 = true;
    this.isShow = !this.isShow;
  }
  toggleDisplay2() {
    this.isShow3 = true;
    this.isShow = true;
    this.isShow2 = !this.isShow2;
  }
  toggleDisplay3() {
    this.isShow = true;
    this.isShow2 = true;
    this.isShow3 = !this.isShow3;
  }


  ngOnInit(): void {
  }

}
