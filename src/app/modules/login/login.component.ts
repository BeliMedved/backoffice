import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  constructor(private router: Router, ) { }

  ngOnInit(): void {
  }
  LoginUser(){
    if(this.email === 'admin@urbateur.com' && this.password === 'admin'){
      this.router.navigate(['/']);
    }
  }

}
